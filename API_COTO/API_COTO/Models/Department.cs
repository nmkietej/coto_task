﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class Department
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public string Notes { get; set; }
        public string MailGroup { get; set; }
        public string Icon { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
